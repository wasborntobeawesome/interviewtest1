//
//  UserDefualts.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import UIKit
extension UserDefaults {
    class func customATObjectForKey(forKey key: String) -> Any? {
        return UserDefaults.standard.object(forKey: key)
    }
    class func setCustomATObject(_ object: Any?, forKey key: String?) {
        if key == nil {
            return
        }
        if object == nil {
            UserDefaults.standard.removeObject(forKey: key!)
        } else {
            UserDefaults.standard.set(object, forKey: key!)
        }
        UserDefaults.standard.synchronize()
    }
    class func removeCustomATObjectForKey(forKey key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
}
class AppManager {
    static let shared = AppManager()
    func setArenaData(models: [ArenaModel]) {
        for model in models {
            if let number = model.number {
                UserDefaults.setCustomATObject(model.idName, forKey: "\(number)")
            }
        }
    }
    func getArenaId(number: Int) -> String? {
        return UserDefaults.customATObjectForKey(forKey: "\(number)") as? String
    }
}
