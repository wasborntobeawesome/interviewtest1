//
//  AppDelegate.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/1/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//

import UIKit
import Reqres
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization Bundleafter application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let viewController = UIStoryboard(name: MainTabBarController.className, bundle: nil)
        window?.rootViewController = viewController.instantiateInitialViewController()
        Reqres.register()
        return true
    }
}
