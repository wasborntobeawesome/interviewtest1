//
//  CardsViewModel.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
import Moya
import RxCocoa
import RxSwift
class CardsViewModel {
    init(endpoint: CardsEndpoint) {
        self.endpoint = endpoint
    }
    var endpoint: CardsEndpoint
    let provider = MoyaProvider<CardsEndpoint>()
    let items: BehaviorRelay<[CardsModel]> = BehaviorRelay(value: [])
    let showLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)//Variable<[ArenaModel]>([])
    func fetch() {
        showLoading.accept(true)
        print("asdasd")
        //guard let safeEndpoint = endpoint else { return }
        provider.request(endpoint) { [weak self] result in
            //guard self != nil else { return }
            guard let weakSelf = self else { return }
            weakSelf.showLoading.accept(false)
            switch result {
            case .failure(let error):
                print(error)
            case .success(let value):
                do {
                    let data = try JSONDecoder().decode([CardsModel].self, from: value.data)
                    weakSelf.items.accept([])
                    weakSelf.items.accept(data)
                    // self!.item.value.append(data)
                    } catch let error {
                    print(error)
                }
            }
        }
    }
}
