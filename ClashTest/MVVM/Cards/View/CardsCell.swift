//
//  CardsCell.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//

import UIKit

class CardsCell: UICollectionViewCell {
    @IBOutlet weak var cardsImage: UIImageView!
    var model: CardsModel? {
        didSet {
            let base = "http://www.clashapi.xyz/" + "images/cards/" + "\(model?.idName ?? "").png"
            //print(model)
            cardsImage?.kf.indicatorType = .activity
            cardsImage?.kf.setImage(with: URL(string: base))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundView?.backgroundColor = .red
        // Initialization code
    }
}
