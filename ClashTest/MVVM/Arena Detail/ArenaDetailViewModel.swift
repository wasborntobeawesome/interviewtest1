//
//  ArenaDetailViewModel.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
import Moya
class ArenaDetailViewModel {
    var isLoading: Bool = false {
        didSet {
            showLoading?()
        }
    }
    var showLoading: (() -> Void)?
    var getData: ((_ data: Int) -> Void)?
    var showError: ((_ text: String) -> Void)?
    let provider = MoyaProvider<CardsEndpoint>()
    var items = [CardsModel]() {
        didSet {
            print(items.count)
        }
    }
    var element: ArenaModel? {
        didSet {
            if let cards = element?.cardUnlocks {
                for card in cards {
                    fetch(card)
                }
            }
        }
    }
    func fetch(_ card: String) {
        isLoading = true
        provider.request(CardsEndpoint.cardsId(id: card)) { [weak self] result in
            guard let weakSelf = self else {
                return
            }
            weakSelf.isLoading = false
            switch result {
            case .failure(let error):
                print(error)
            case .success(let value):
                do {
                    let data = try JSONDecoder().decode(CardsModel.self, from: value.data)
                    weakSelf.items.append(data)
                    weakSelf.getData?(weakSelf.items.count)
                } catch let error {
                    print(error)}
            }
        }
    }
}
