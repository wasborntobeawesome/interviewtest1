//
//  ArenaDetailController.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import UIKit
class ArenaDetailController: UIViewController {
    var cardFlag = true
    var cardsModel: CardsModel? {
        didSet {
            self.title = cardsModel?.idName
            self.navigationController?.navigationBar.topItem?.title = cardsModel?.idName
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationItem.largeTitleDisplayMode = .always
            } else {
            }
        }
    }
    var arenaModel: ArenaModel? {
        didSet {
            self.title = arenaModel?.idName
            self.navigationController?.navigationBar.topItem?.title = arenaModel?.idName
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationItem.largeTitleDisplayMode = .always
            } else {
            }
            viewModel.element = arenaModel
        }
    }
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    let viewModel = ArenaDetailViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: ArenaHeaderView.className, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ArenaHeaderView.className)
        collectionView.register(UINib(nibName: CardsCell.className, bundle: nil), forCellWithReuseIdentifier: CardsCell.className)
        viewModel.getData = {
            data in
            print(data)
            if let count = self.arenaModel?.cardUnlocks?.count {
                if count == data {
                    print("asdas")
                    self.collectionView.reloadData()
                }
            }
        }
        viewModel.showLoading = {
            if self.viewModel.isLoading {
                self.activityLoader.startAnimating()
            } else {
                self.activityLoader.stopAnimating()
            }
        }
    }
}
extension ArenaDetailController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if cardFlag {
            return CGSize(width: view.frame.width, height: 240)
        } else {
            let width = collectionView.bounds.width
            var cellWidth: CGFloat = (width - 10) / 2
            if UIDevice.current.orientation.isLandscape {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cellWidth = (width - (10*5)) / 5
                } else {
                    cellWidth = (width - 20) / 3
                }
            } else {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cellWidth = (width - 10*3) / 3
                } else {
                    cellWidth = (width - 20) / 2
                }
            }
            return CGSize(width: cellWidth, height: cellWidth / 0.9)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 250)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if let safeCollectionView = collectionView {
            safeCollectionView.reloadData()
        }
    }
}
extension ArenaDetailController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if cardFlag {
            return 1
        } else {
            return viewModel.items.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardsCell.className, for: indexPath) as? CardsCell else {
            return UICollectionViewCell()
        }
        if cardFlag {
            let base = "http://www.clashapi.xyz/" + "images/arenas/" + "\(AppManager.shared.getArenaId(number: cardsModel?.arena ?? 0) ?? "").png"
            cell.cardsImage.kf.indicatorType = .activity
            cell.cardsImage.kf.setImage(with: URL(string: base))
        } else {
            cell.model = viewModel.items[indexPath.item]
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ArenaHeaderView.className, for: indexPath) as? ArenaHeaderView else {
            return UICollectionReusableView()
        }
        var base: String
        if cardFlag {
            base = "http://www.clashapi.xyz/" + "images/cards/" + "\(cardsModel?.idName ?? "").png"
        } else {
            base = "http://www.clashapi.xyz/" + "images/arenas/" + "\(arenaModel?.idName ?? "").png"
        }
        if let imageView = header.headerImage {
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: URL(string: base))
        }
        return header
    }
}
