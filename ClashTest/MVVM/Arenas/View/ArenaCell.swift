//
//  ArenaCell.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/7/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//

import UIKit
import Kingfisher
class ArenaCell: UITableViewCell {
    @IBOutlet weak var arenaImage: UIImageView!
    var test: String? {
        didSet {
            print(test ?? "error in test")
        }
    }
    var model: ArenaModel? {
        didSet {
            let base = "http://www.clashapi.xyz/" + "images/arenas/" + "\(model?.idName ?? "").png"
            print(base)
            arenaImage?.kf.indicatorType = .activity
            arenaImage?.kf.setImage(with: URL(string: base))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
