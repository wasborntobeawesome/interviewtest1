//
//  ArenasViewModel.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/7/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
import RxSwift
import RxCocoa
import Moya
class ArenasViewModel {
    let provider = MoyaProvider<ArenasEndpoint>()
    let items: BehaviorRelay<[ArenaModel]> = BehaviorRelay(value: [])
    let showLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    func fetch() {
        showLoading.accept(true)
        provider.request(ArenasEndpoint.arenas) { [weak self] result in
            guard let weakSelf = self else { return }
            weakSelf.showLoading.accept(false)
            switch result {
            case .failure(let error):
                print(error)
            case .success(let value):
                do {
                    let data = try JSONDecoder().decode([ArenaModel].self, from: value.data)
                    AppManager.shared.setArenaData(models: data)
                    weakSelf.items.accept(weakSelf.items.value + data)
                    } catch let error {
                    print(error)
                }
            }
        }
    }
}
