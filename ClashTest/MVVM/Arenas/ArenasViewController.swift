//
//  ArenasViewController.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/3/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
class ArenasViewController: UIViewController {
    let disposeBag = DisposeBag()
    let viewModel = ArenasViewModel()
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Arenas"
        self.navigationController?.navigationBar.topItem?.title = "Arenas"
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationItem.largeTitleDisplayMode = .always
        } else {
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: ArenaCell.className, bundle: nil), forCellReuseIdentifier: ArenaCell.className)
        viewModel.items
        .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: ArenaCell.className, cellType: ArenaCell.self)) { _, viewModel, cell in
                cell.model = viewModel
        }
        .disposed(by: disposeBag)
        viewModel.fetch()
        viewModel.showLoading.subscribe { [weak self] (event) in
            guard let weakSelf = self else { return }
            if let show = event.element {
                if show {
                    weakSelf.activityLoader.startAnimating()
                } else {
                    weakSelf.activityLoader.stopAnimating()
                }
            }
            }.disposed(by: disposeBag)
        tableView.rx.modelSelected(ArenaModel.self).subscribe { [weak self] (event) in
            guard let weakSelf = self else { return }
            let storyboard = UIStoryboard(name: ArenaDetailController.className, bundle: nil)
            guard let viewController = storyboard.instantiateInitialViewController() as? ArenaDetailController else {
                return
            }
            //viewController.modelTitle  = event.element?.idName
            viewController.arenaModel = event.element
            viewController.cardFlag = false
            //let viewController = iewController.instantiateInitialViewController()
            weakSelf.navigationController?.pushViewController(viewController, animated: true)
            }
            .disposed(by: disposeBag)
        tableView.rx.itemSelected.subscribe { [weak self] (event) in
            guard let weakSelf = self, let indexPath = event.element else { return }
            weakSelf.tableView.deselectRow(at: indexPath, animated: true)
            }
            .disposed(by: disposeBag)
    }
}
