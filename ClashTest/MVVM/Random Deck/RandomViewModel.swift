//
//  RandomViewModel.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
import Moya
import RxCocoa
import RxSwift
class RandomViewModel {
    let provider = MoyaProvider<CardsEndpoint>()
    let items: BehaviorRelay<[CardsModel]> = BehaviorRelay(value: [])//Variable<[ArenaModel]>([])
    func fetch() {
        provider.request(CardsEndpoint.random) { [weak self] result in
            //guard self != nil else { return }
            guard let weakSelf = self else { return }
            switch result {
            case .failure(let error):
                print(error)
            case .success(let value):
                do {
                    let data = try JSONDecoder().decode([CardsModel].self, from: value.data)
                    weakSelf.items.accept(weakSelf.items.value + data)
                    } catch _ {
                    }
            }
        }
    }
}
