//
//  RandomViewController.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/7/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import UIKit
import RxCocoa
import RxSwift
class RandomViewController: UIViewController {
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    let refreshControl = UIRefreshControl()
    let disposeBag = DisposeBag()
    let viewModel = CardsViewModel(endpoint: CardsEndpoint.random)
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Random"
        self.navigationController?.navigationBar.topItem?.title = "Random"
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: CardsCell.className, bundle: nil), forCellWithReuseIdentifier: CardsCell.className)
        collectionView.refreshControl = refreshControl
        viewModel.items
            .asObservable()
            .bind(to: collectionView.rx.items(cellIdentifier: CardsCell.className, cellType: CardsCell.self)) { _, viewModel, cell in
                cell.model  = viewModel
            }
            .disposed(by: disposeBag)
        viewModel.fetch()
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        viewModel.showLoading.subscribe { [weak self] (event) in
            guard let weakSelf = self else { return }
            if let show = event.element {
                if show {
                    weakSelf.activityLoader.startAnimating()
                } else {
                    weakSelf.activityLoader.stopAnimating()
                }
            }
            }.disposed(by: disposeBag)
        collectionView.rx.modelSelected(CardsModel.self).subscribe { [weak self] event in
            guard let weakSelf = self else { return }
            let storyboard = UIStoryboard(name: ArenaDetailController.className, bundle: nil)
            guard let viewController = storyboard.instantiateInitialViewController() as? ArenaDetailController else {
                return
            }
            viewController.cardsModel = event.element
            weakSelf.navigationController?.pushViewController(viewController, animated: true)
            }.disposed(by: disposeBag)
    }
}
extension RandomViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        var cellWidth: CGFloat = (width - 10) / 2
        if UIDevice.current.orientation.isLandscape {
            if UIDevice.current.userInterfaceIdiom == .pad {
                cellWidth = (width - (10*5)) / 5
            } else {
                cellWidth = (width - 20) / 3
            }
        } else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                cellWidth = (width - 10*3) / 3
            } else {
                cellWidth = (width - 20) / 2
            }
        }
        return CGSize(width: cellWidth, height: cellWidth / 0.9)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if let safeCollectionView = collectionView {
            safeCollectionView.reloadData()
        }
    }
}
