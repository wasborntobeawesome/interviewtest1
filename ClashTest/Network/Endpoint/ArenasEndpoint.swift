//
//  ArenasEndpoint.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/3/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
import Moya
enum ArenasEndpoint {
    case arenas
}
extension ArenasEndpoint: TargetType {
    var path: String {
        return "api/arenas"
    }
    var sampleData: Data {
        return Data()
    }
    var task: Task {
        return .requestPlain
    }
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}
