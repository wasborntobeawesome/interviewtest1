//
//  CardsEndpoint.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/8/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
import Moya
enum CardsEndpoint {
    case cards
    case random
    case cardsId(id: String)
}
extension CardsEndpoint: TargetType {
    var path: String {
        switch self {
        case .cards:
            return "api/cards"
        case .random:
            return "api/random-deck"
        case .cardsId(let id):
            return "api/cards/" + id
        }
    }
    var sampleData: Data {
        return Data()
    }
    var task: Task {
        return .requestPlain
    }
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}
