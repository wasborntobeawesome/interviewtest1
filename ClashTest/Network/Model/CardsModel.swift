//
//  CardsModel.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/3/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
struct CardsModel: Codable {
    let id: String?
    let idName: String?
    let rarity: String?
    let type: String?
    let name: String?
    let description: String?
    let elixirCost: Int?
    let copyId: Int?
    let arena: Int?
    let order: Int?
    let doubleUndercoreV: Int?
}
extension CardsModel {
    enum CodingKeys: String, CodingKey {
        case id  = "_id"
        case idName
        case rarity
        case type
        case name
        case description
        case elixirCost
        case copyId
        case arena
        case order
        case doubleUndercoreV = "__v"
    }
}
