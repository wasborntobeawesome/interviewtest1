//
//  ArenaModel.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/3/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Foundation
struct ArenaModel: Codable {
    let id: String?
    let idName: String?
    let number: Int?
    let name: String?
    let victoryGold: Int?
    let minTrophies: Int?
    let order: Int?
    let doubleUndercoreV: Int?
    let leagues: [String]?
    let cardUnlocks: [String]?
    let chests: [String]?
    let clan: ClanModel?
}
struct ClanModel: Codable {
    let donate: DonateRequest?
    let request: DonateRequest?
}
struct DonateRequest: Codable {
    let common: Int?
    let rare: Int?
}
extension ArenaModel {
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case idName
        case number
        case name
        case victoryGold
        case minTrophies
        case order
        case doubleUndercoreV = "__v"
        case leagues
        case cardUnlocks
        case chests
        case clan
    }
}
