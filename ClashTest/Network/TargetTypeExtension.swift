//
//  TargetTypeExtension.swift
//  ClashTest
//
//  Created by Alimov Islom on 4/3/19.
//  Copyright © 2019 Alimov Islom. All rights reserved.
//
import Moya
import Foundation
extension TargetType {
    var baseURL: URL {
        return URL(string: "http://www.clashapi.xyz/")!
    }
    var method: Moya.Method {
        return .get
    }
     var validationType: ValidationType {
        return .successCodes
    }
}
