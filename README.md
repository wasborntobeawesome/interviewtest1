# Test - Clash Royale simple browser

## Goal

Create a simple browser for Clash Royale data (*cards* and *arenas*) using data from open APIs

APIs can be found [here](https://github.com/martincarrera/clash-royale-api)

Base-URL is http://www.clashapi.xyz/

The app should display:

- A `UITabBarController` with 3 scenes: Arenas, Cards and RandomDeck

- A `ArenasViewController` with all the arenas in a vertical list (one element per row) -> [API](http://www.clashapi.xyz/api/arenas)

- A `CardsViewController` with a grid of cards (2 cards per row) -> [API](http://www.clashapi.xyz/api/cards)

- A `ArenaDetailViewController`, showing the main image of the arena, its title and a list of unlocked cards

- A `CardDetailViewController`, showing the main image of the card, its title and related arena image

- The RandomDeck scene should display a random deck (2 cards per row) -> [API](http://www.clashapi.xyz/api/random-deck)


## Bonus

- Unlock orientations and display different number of columns between landscape and portrait

- Make the app universal and display different number of columns on iPad

## Technical constraints:

- Use [Moya](https://github.com/Moya/Moya) for networking

- Use native `Codable` objects to handle data

- Use LayoutConstraints in interface builder (either storyboards or xibs)

## Optional constraints

These constraints are optional but are used in all of our projects.

- Use [RxSwift](https://github.com/ReactiveX/RxSwift) with Moya to handle networking and UI refresh

- Use [AlamofireImage](https://github.com/Alamofire/AlamofireImage) to handle image download.

- Use MVVM architecture

- Reuse views and logic (example: RandomDeck is *almost* identical to Cards list)

- Xibs are generally preferred to storyboards

- Use functional programming concepts whenever possible (`map`,`filter`,`compactMap`, etc...)
